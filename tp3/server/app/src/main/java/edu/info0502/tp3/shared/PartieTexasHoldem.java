package edu.info0502.tp3.shared;

import java.util.ArrayList;

public class PartieTexasHoldem {
    private ArrayList<Joueur> joueurs;
    private Talon talon;
    private ArrayList<Carte> cartesCommunes;

    public PartieTexasHoldem(ArrayList<Joueur> joueurs) {
        this.joueurs = joueurs;
        this.talon = new Talon();
        talon.melanger();
        this.cartesCommunes = new ArrayList<>();
    }

    public void distribuerCartesCachees() {
        for (Joueur joueur : joueurs) {
            joueur.ajouterCarteCachee(talon.tirerCarte());
            joueur.ajouterCarteCachee(talon.tirerCarte());
        }
    }

    public void devoilerFlop(){
        // Dévoiler trois cartes pour le flop
        cartesCommunes.add(talon.tirerCarte());
        cartesCommunes.add(talon.tirerCarte());
        cartesCommunes.add(talon.tirerCarte());
        System.out.println("Flop: " + cartesCommunes);
    }

    public void devoilerTurn() {
        // Dévoiler une carte pour le turn
        cartesCommunes.add(talon.tirerCarte());
        System.out.println("Turn: " + cartesCommunes.get(cartesCommunes.size() - 1));
    }
    
    public void devoilerRiver() {
        // Dévoiler la dernière carte pour la river
        cartesCommunes.add(talon.tirerCarte());
        System.out.println("River: " + cartesCommunes.get(cartesCommunes.size() - 1));
    }

    public void evaluerMains() {
        for (Joueur joueur : joueurs) {
            joueur.evaluerMain(cartesCommunes); // Appelle evaluerMain avec les cartes communes
        }
    }

    public ArrayList<Carte> getCartesCommunes() {
        return cartesCommunes;
    }
}
