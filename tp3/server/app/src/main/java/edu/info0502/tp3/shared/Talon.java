package edu.info0502.tp3.shared;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;

public class Talon implements Serializable {
    private ArrayList<Paquet> paquets;

    public Talon() {
        paquets = new ArrayList<>();
        ajouterPaquet(new Paquet());
    }

    public Talon(int n) {
        this.paquets = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            ajouterPaquet(new Paquet());
        }
    }
    
    public void ajouterPaquet(Paquet paquet) {
        paquets.add(paquet);
    }

    public ArrayList<Paquet> getPaquets() {
        return paquets;
    }

    public void melanger() {
        for (Paquet paquet : paquets) {
            paquet.melanger();
        }
        Collections.shuffle(paquets);
    }

    public Carte tirerCarte() {
        for (Paquet paquet : paquets) {
            Carte carte = paquet.tirerCarte();
            if (carte != null) return carte;
        }
        return null;
    }

    public void afficherTalon() {
        for (Paquet paquet : paquets) {
            paquet.afficherPaquet();
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Paquet paquet : paquets) {
            sb.append(paquet.toString()).append("\n");
        }
        return sb.toString();
    }
}
