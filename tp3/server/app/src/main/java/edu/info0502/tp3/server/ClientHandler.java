package edu.info0502.tp3.server;

import java.io.*;
import java.net.Socket;

import edu.info0502.tp3.shared.Joueur;
import edu.info0502.tp3.shared.Message;

public class ClientHandler implements Runnable {
    private final Socket socket;
    private final ServeurPoker serveur;
    
    private Joueur joueur;

    private ObjectInputStream inputStream;
    private ObjectOutputStream outputStream;
    
    private boolean isrunning;
    private int i;

    public ClientHandler(Socket socket, ServeurPoker serveur) {
        this.socket = socket;
        this.serveur = serveur;
        isrunning = true;
        try {
            outputStream = new ObjectOutputStream(socket.getOutputStream());
            inputStream = new ObjectInputStream(socket.getInputStream());
        } catch (IOException e) {
            throw new RuntimeException("Erreur avec l'init des flux", e);
        }
    }

    // Getter du joueur
    public Joueur getJoueur() {
        return joueur; 
    }

    // Setter du joueur
    public void setJoueur(Joueur j){
        joueur = j;
    }

    @Override
    public void run() {
        try {
            while (isrunning) {
                // Lecture du message envoyé par le client (castage en instance Message)
                Message message = (Message) inputStream.readObject();
                // Traitement dudit message
                traiterMessage(message);
            }
        } catch (EOFException e) {
                System.err.println("Client déconnecté : " + joueur.getNom());
        } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
        } finally {
                // Fermer proprement la connexion
                fermerConnexion();
        }
        
    }

    public void envoyerMessage(Message message) {
        try {
            outputStream.writeObject(message);
            outputStream.flush();
        } catch (IOException e) {
            System.err.println("Erreur d'envoi au client " + joueur.getNom());
        }
    }

    private void traiterMessage(Message message) {
        switch (message.getType()) {
            // Si le message est une authentification, le client enverra un pseudo
            case Message.TypeMessage.AUTH:
                String nickname = (String)message.getContenu();
                // Si le pseudo est vide
                if (nickname == "") {
                    // Par défaut, il sera "Joueur n" (où n est un nombre positif entre 0 et +infini)
                    nickname = "Joueur " + i++;
                }
                // Sinon si le pseudo est utilisé
                else if (!serveur.verifierPseudo(nickname)) {
                    envoyerMessage(new Message(Message.TypeMessage.AUTH_FAIL, "Pseudo déjà utilisé par un autre joueur"));
                    // On lui donne un pseudo par défaut
                    nickname = "Joueur " + i++;
                } 
                // Sinon, le joueur est connecté et avec le pseudo qu'il a choisit
                else {
                    Joueur nouveauJoueur = new Joueur(nickname);
                    setJoueur(nouveauJoueur); // On change la variable de gestion du joueur du handler
                    serveur.ajouterClient(this); // On l'ajoute au serveur
                    envoyerMessage(new Message(Message.TypeMessage.AUTH_SUCC, "Pseudo validé"));
                    envoyerMessage(new Message(Message.TypeMessage.AUTH_SUCC, "Bienvenue dans le hub de la partie de poker !"));
                }
                
                break;
            // Si le message est une demande pour commencer la partie
            case Message.TypeMessage.START_POKER:
                if (serveur.getClients().size()>=2) {
                    serveur.debuterPartie();
                } else {
                    envoyerMessage(new Message(Message.TypeMessage.ERROR, "Démarrage de la partie impossible\nNombre de clients : "+serveur.getClients().size()));
                }
                break;
            // Si le message est une demande de déconnexion
            case Message.TypeMessage.DISCONNECT:
                isrunning = false; // Fin de la partie pour le joueur (arrêt de la boucle de jeu)
                serveur.retirerClient(this); // Joueur retiré du serveur
                envoyerMessage(new Message(Message.TypeMessage.DISCONNECT_SUCC, "Vous avez quitté la partie"));
                break;
            // Cas par défaut
            default:
                envoyerMessage(new Message(Message.TypeMessage.ERROR, "Message inconnu"));
        }
    }

    private void fermerConnexion() {
        try {
            isrunning = false;
            if (joueur != null) {
                serveur.retirerClient(this);
                System.out.println("Joueur déconnecté : " + joueur.getNom());
            }
            if (socket != null) socket.close();
            if (inputStream != null) inputStream.close();
            if (outputStream != null) outputStream.close();
        } catch (IOException e) {
            System.err.println("Erreur lors de la fermeture de la connexion : " + e.getMessage());
        }
    }
}
