package edu.info0502.tp3.server;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import edu.info0502.tp3.shared.Carte;
import edu.info0502.tp3.shared.Joueur;
import edu.info0502.tp3.shared.Message;
import edu.info0502.tp3.shared.Main;
import edu.info0502.tp3.shared.Talon;

public class ServeurPoker {
    private Main cartesCommunes;
    private final int port;
    private ArrayList<ClientHandler> clients;
    private boolean partieEnCours;

    public ServeurPoker(int port) {
        this.port = port;
        clients = new ArrayList<>();
        partieEnCours = false;
    }

    public void lancer() {
        try (ServerSocket serverSocket = new ServerSocket(this.port)) {
            System.out.println("Serveur lancé sur le port " + port);
    
            while (true) {
                try {
                    Socket clientSocket = serverSocket.accept();
                    if(clients.size()<10){
                        ClientHandler clientHandler = new ClientHandler(clientSocket, this);
                        System.out.println("Nouveau client connecté : " + clientSocket.getInetAddress());
                        new Thread(clientHandler).start();
                    }
                    else{
                        System.out.println("Connexion refusée : table pleine (plus de 10 joueurs)");
                        clientSocket.close();
                    }
                } catch (IOException e) {
                    System.err.println("Erreur lors de la gestion de la connexion d'un client : " + e.getMessage());
                }
            }
        } catch (IOException e) {
            System.err.println("Erreur serveur : " + e.getMessage());
        }
    }

    public synchronized boolean verifierPseudo(String nickname){
        return clients.stream().noneMatch(c -> c.getJoueur().getNom().equals(nickname));
    } 

    // Ajout d'un client
    // TO-DO a revoir
    public synchronized void ajouterClient(ClientHandler clientHandler) {
        if (verifierPseudo(clientHandler.getJoueur().getNom())) {
            clients.add(clientHandler);
            System.out.println("Client " + clientHandler.getJoueur().getNom() + " ajouté.");
        }
    }

    // Retirer un joueur
    public synchronized void retirerClient(ClientHandler clientHandler) {
        clients.remove(clientHandler);
        System.out.println("Client " + clientHandler.getJoueur().getNom() + " retiré");
    }

    // Débuter la partie
    public synchronized void debuterPartie() {
        if(clients.size() < 2) {
            System.out.println("Pas assez de joueurs pour débuter la partie.");
            return;
        }
        if(partieEnCours){
            System.out.println("Une partie est déjà en cours.");
            return;  
        }

        partieEnCours = true;
        Talon talon = new Talon(1);
        talon.melanger();

        // Distribution de deux cartes pour chaque joueur
        for (ClientHandler client : clients) {
            Carte carte1 = talon.tirerCarte();
            client.getJoueur().ajouterCarteCachee(carte1);
            
            Carte carte2 = talon.tirerCarte();
            client.getJoueur().ajouterCarteCachee(carte2);
            
            // Envoie de message aux clients
            client.envoyerMessage(new Message(Message.TypeMessage.CARD_DISTRIBUTION, carte1));
            client.envoyerMessage(new Message(Message.TypeMessage.CARD_DISTRIBUTION, carte2));
        }

        System.out.println("Cartes cachées envoyées aux joueurs");

        // Flop (3 cartes distribuées)
        cartesCommunes = new Main(talon, 3);
        // Turn (Une autre carte)
        cartesCommunes.ajouterCarte(talon.tirerCarte()); 
        // River (le reste des cartes, 1 carte)
        cartesCommunes.ajouterCarte(talon.tirerCarte());


        // On place les cartes dans une liste pour mieux les gérer
        List<Carte> cartes = new ArrayList<>();
        cartes = cartesCommunes.getCartes();
        for(Carte carte : cartes){
            notifierTous(new Message(Message.TypeMessage.FLOP, carte));
        }

        System.out.println("Affichage des cartes effectué");
        calculerEtEnvoyerResultats();
    }

    private void calculerEtEnvoyerResultats() {
        // Liste de mains des joueurs 
        ArrayList<Main> mainsJoueurs = new ArrayList<>();
        for (ClientHandler client : clients) {
            Joueur joueur = client.getJoueur();
            Main mainJoueur = new Main(cartesCommunes);
            mainJoueur.ajouterCartes(joueur.getCartesCachees());
            mainsJoueurs.add(mainJoueur);
        }

        Main mainPuissante = mainsJoueurs.get(0);
        int win = 0;
        for (int i = 1; i < mainsJoueurs.size(); i++) {
            if (mainsJoueurs.get(i).comparerMains(mainPuissante)) {
                mainPuissante = mainsJoueurs.get(i);
                win = i;
            }
        }

        // Réveler les mains des joueurs
        StringBuilder resultats = new StringBuilder();
        for (int i = 0; i < clients.size(); i++) {
            Joueur joueur = clients.get(i).getJoueur();
            Main mainJoueur = mainsJoueurs.get(i);
            resultats.append("Main de ").append(joueur.getNom()).append(" : \n").append(mainJoueur.retournerMain()).append("\nCombinaison : ").append(mainsJoueurs.get(i).evaluerCombinaison()).append("\n");
        }
        notifierTous(new Message(Message.TypeMessage.RESULTS, resultats.toString()));

        System.out.println("Mains distribuées");
        
        //Affichage du vainqueur
        resultats = new StringBuilder();
        ClientHandler joueurwin = clients.get(win);
        resultats.append("\nVainqueur : ").append(joueurwin.getJoueur().getNom());
        notifierTous(new Message(Message.TypeMessage.GAME_RESULT, resultats.toString()));

        System.out.println("Fin de la partie, Gagnant : " + joueurwin.getJoueur().getNom());
        System.out.println();
        partieEnCours = false;
    }

    // Notifier tous les clients avec un message
    public synchronized void notifierTous(Message message) {
        List<ClientHandler> clientsDeconnectes = new ArrayList<>();
        for (ClientHandler client : clients) {
            try {
                client.envoyerMessage(message);
            } catch (Exception e) {
                System.err.println("Erreur lors de l'envoi du message au client " + client.getJoueur().getNom() + ": " + e.getMessage());
                clientsDeconnectes.add(client);
            }
        }
        // Retirer les clients déconnectés
        for (ClientHandler client : clientsDeconnectes) {
            retirerClient(client);
        }
    }

    public ArrayList<ClientHandler> getClients(){
        return clients;
    }
}
