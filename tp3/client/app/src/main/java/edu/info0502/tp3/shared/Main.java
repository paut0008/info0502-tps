package edu.info0502.tp3.shared;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class Main implements Serializable {
    private ArrayList<Carte> cartes = new ArrayList<>();

    /*
     * Constructeur de la Main
     */
    public Main(Talon talon) {
        cartes = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            Carte carte = talon.tirerCarte();
            if (carte != null) {
                cartes.add(carte);
            }
        }
    }


    /*
     * Constructeur par défaut
     */
    public Main() {
        cartes = new ArrayList<>(5); // Une main a 5 cartes
    }


    public Main(Main main) {
        cartes = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            Carte carte = main.cartes.get(i);
            if (carte != null) {
                this.cartes.add(carte);
            }
        }
    }

    public Main(Talon talon, int taille) {
        cartes = new ArrayList<>();
        for (int i = 0; i < taille; i++) {
            Carte carte = talon.tirerCarte();
            if (carte != null) {
                cartes.add(carte);
            }
        }
    }

    /*
     * Méthode pour afficher la main
     */
    public String retournerMain() {
        String res = "";
        for (Carte carte : cartes) {
            res += carte+" ";
        }
        return res;
    }

    public void ajouterCarte(Carte carte){
        this.cartes.add(carte);
    }

    public void ajouterCartes(ArrayList<Carte> carte){
        for (int i = 0; i < carte.size(); i++) {
            this.cartes.add(carte.get(i));
        }
    }

    public ArrayList<Carte> getCartes(){
        return cartes;
    }

    /*
     * Méthode pour trier la main par ordre croissant
     */
    private void trierMain() {
        cartes.sort(Comparator.comparingInt(Carte::getNombre));
    }

    /*
     * Méthode pour évaluer la combinaison d'une main
     */
    public String evaluerCombinaison() {
        //On trie la main
        trierMain(); 

        if (estQuinteFlushRoyale()) return "Quinte Flush Royale";
        if (estQuinteFlush()) return "Quinte Flush";
        if (estCarre()) return "Carré";
        if (estFull()) return "Full";
        if (estCouleur()) return "Couleur";
        if (estQuinte()) return "Quinte";
        if (estBrelan()) return "Brelan";
        if (estDoublePaire()) return "Double Paire";
        if (estPaire()) return "Paire";

        // Main triée : le dernier élément est le plus haut
        return "Carte la plus haute : " + cartes.get(cartes.size() - 1).getNombre() + " de " + cartes.get(cartes.size() - 1).getSigne();
    }

    /*
     * Méthode pour déterminer si la main est une quinte flush royale
     */
    private boolean estQuinteFlushRoyale() {
        return estQuinteFlush() && cartes.get(0).getNombre() == 10;
    }

    /*
     * Méthode pour déterminer si la main est une quinte flush
     */
    private boolean estQuinteFlush() {
        return estQuinte() && estCouleur();
    }

    /*
     * Méthode pour déterminer si la main est un carré (quatre cartes identiques)
     */
    private boolean estCarre() {
        int[] frequences = calculerFrequences();
        for (int frequence : frequences) {
            if (frequence == 4) return true;
        }
        return false;
    }

    /*
     * Méthode pour déterminer si la main est un full (brelan + paire)
     */
    private boolean estFull() {
        boolean brelanBool = false;
        boolean paireBool = false;
        int[] frequences = calculerFrequences();
        for (int frequence : frequences) {
            if (frequence == 3) brelanBool = true;
            if (frequence == 2) paireBool = true;
        }
        return brelanBool && paireBool;
    }

    /*
     * Méthode pour déterminer si la main est une couleur (toutes les cartes de la même couleur)
     */
    private boolean estCouleur() {
        Carte.SIGNE couleur = cartes.get(0).getSigne();
        for (Carte carte : cartes) {
            if (carte.getSigne() != couleur) return false;
        }
        return true;
    }

    /*
     * Méthode pour déterminer si la main est une quinte (5 cartes qui se suivent)
     */
    private boolean estQuinte() {
        for (int i = 0; i < cartes.size() - 1; i++) {
            if (cartes.get(i).getNombre() + 1 != cartes.get(i + 1).getNombre()) {
                return false;
            }
        }
        return true;
    }

    /*
     * Méthode pour déterminer si la main est un brelan (trois cartes identiques)
     */
    private boolean estBrelan() {
        int[] frequences = calculerFrequences();
        for (int frequence : frequences) {
            if (frequence == 3) return true;
        }
        return false;
    }

    /*
     * Méthode pour déterminer si la main a deux paires
     */
    private boolean estDoublePaire() {
        int nombreDePaires = 0;
        int[] frequences = calculerFrequences();
        for (int frequence : frequences) {
            if (frequence == 2) nombreDePaires++;
        }
        return nombreDePaires == 2;
    }

    /*
     * Méthode pour déterminer si la main a une paire
     */
    private boolean estPaire() {
        int[] frequences = calculerFrequences();
        for (int frequence : frequences) {
            if (frequence == 2) return true;
        }
        return false;
    }

    /*
     * Méthode utilitaire pour calculer la fréquence des valeurs des cartes
     */
    private int[] calculerFrequences() {
        int[] frequences = new int[14];  
        for (Carte carte : cartes) {
            frequences[carte.getNombre()]++;
        }
        return frequences;
    }

    /*
     * Méthode pour comparer deux mains
     */
    public boolean comparerMains(Main main) {
        String combinaison1 = this.evaluerCombinaison();
        String combinaison2 = main.evaluerCombinaison();
    
        // Ordre des combinaisons par force
        List<String> ordreDesCombinaisons = Arrays.asList(
            "Carte la plus haute", "Paire", "Double Paire", "Brelan", "Quinte",
            "Couleur", "Full", "Carré", "Quinte Flush", "Quinte Flush Royale"
        );
    
        // Obtenir la valeur des combinaisons
        int valeurMain1 = ordreDesCombinaisons.indexOf(combinaison1);
        int valeurMain2 = ordreDesCombinaisons.indexOf(combinaison2);
    
        // Retourner si la main actuelle est plus forte
        return valeurMain1 > valeurMain2;
    }
}

