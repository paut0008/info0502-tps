package edu.info0502.tp3.shared;

import java.util.ArrayList;
import java.util.List;
import java.io.Serializable;

public class Joueur implements Serializable{
    private String nom;
    private ArrayList<Carte> cartesCachees; // Les 2 cartes personnelles cachées
    private String evaluationMain; // Évaluation de la meilleure combinaison possible

    public Joueur(String nom) {
        this.nom = nom;
        this.cartesCachees = new ArrayList<>(2); // On initialise pour 2 cartes
        this.evaluationMain = "";
    }

    // Ajoute une carte cachée si le joueur en a moins de 2
    public void ajouterCarteCachee(Carte carte) {
        if (cartesCachees.size() < 2) {
            cartesCachees.add(carte);
        }
    }

    // Évaluation de la meilleure combinaison parmi 7 cartes (2 cachées + 5 communes)
    public void evaluerMain(ArrayList<Carte> cartesCommunes) {
        ArrayList<Carte> toutesLesCartes = new ArrayList<>(cartesCachees);
        toutesLesCartes.addAll(cartesCommunes); // Combine cartes cachées et communes

        // Appelle une méthode pour évaluer la main optimale
        this.evaluationMain = evaluerCombinaisonOptimum(toutesLesCartes);
    }

    // Retourne l'évaluation de la main sous forme de texte
    public String getEvaluationMain() {
        return evaluationMain;
    }

    // Évaluation simplifiée de la meilleure combinaison parmi 7 cartes
    private String evaluerCombinaisonOptimum(ArrayList<Carte> cartes) {
        ArrayList<ArrayList<Carte>> combinaisons = genererCombinaisons(cartes, 5);
        String meilleureMain = "";

        for (ArrayList<Carte> combinaison : combinaisons) {
            String evaluation = evaluerCombinaison(combinaison);
            if (estMeilleureCombinaison(evaluation, meilleureMain)) {
                meilleureMain = evaluation;
            }
        }
        return meilleureMain;
    }

    // Génère toutes les combinaisons de 5 cartes parmi les 7
    private ArrayList<ArrayList<Carte>> genererCombinaisons(List<Carte> cartes, int taille) {
        ArrayList<ArrayList<Carte>> result = new ArrayList<>();
        combinaisonsRecursives(result, new ArrayList<>(), cartes, taille, 0);
        return result;
    }

    private void combinaisonsRecursives(ArrayList<ArrayList<Carte>> result, List<Carte> temp, List<Carte> cartes, int taille, int start) {
        if (temp.size() == taille) {
            result.add(new ArrayList<>(temp));
            return;
        }
        for (int i = start; i < cartes.size(); i++) {
            temp.add(cartes.get(i));
            combinaisonsRecursives(result, temp, cartes, taille, i + 1);
            temp.remove(temp.size() - 1);
        }
    }

    // Évalue la force d'une combinaison de 5 cartes de manière simplifiée
    private String evaluerCombinaison(ArrayList<Carte> combinaison) {
        int[] valeurs = new int[13];
        int[] couleurs = new int[4];
        for (Carte carte : combinaison) {
            valeurs[carte.getNombre() - 1]++;
            couleurs[carte.getSigne().ordinal()]++;
        }

        boolean flush = estFlush(couleurs);
        boolean straight = estSuite(valeurs);

        if (flush && straight) return "Quinte Flush";
        if (contientNuplet(valeurs, 4)) return "Carré";
        if (contientNuplet(valeurs, 3) && contientNuplet(valeurs, 2)) return "Full";
        if (flush) return "Couleur";
        if (straight) return "Suite";
        if (contientNuplet(valeurs, 3)) return "Brelan";
        if (nombreDePaires(valeurs) == 2) return "Double Paire";
        if (contientNuplet(valeurs, 2)) return "Paire";
        return "Hauteur";
    }

    private boolean estFlush(int[] couleurs) {
        for (int couleur : couleurs) {
            if (couleur >= 5) return true;
        }
        return false;
    }

    private boolean estSuite(int[] valeurs) {
        int consecutive = 0;
        for (int valeur : valeurs) {
            if (valeur > 0) consecutive++;
            else consecutive = 0;
            if (consecutive >= 5) return true;
        }
        // Vérification de l’As pour la suite basse (As-2-3-4-5)
        return valeurs[0] > 0 && valeurs[12] > 0 && valeurs[1] > 0 && valeurs[2] > 0 && valeurs[3] > 0;
    }

    private boolean contientNuplet(int[] valeurs, int n) {
        for (int valeur : valeurs) {
            if (valeur == n) return true;
        }
        return false;
    }

    private int nombreDePaires(int[] valeurs) {
        int paires = 0;
        for (int valeur : valeurs) {
            if (valeur == 2) paires++;
        }
        return paires;
    }

    // Comparaison des deux évaluations de combinaisons
    private boolean estMeilleureCombinaison(String combinaison1, String combinaison2) {
        // Simplification : retourne true si combinaison1 est mieux classée que combinaison2
        // Logique pour comparer les évaluations de mains (ex : "Quinte Flush" > "Carré")
        return combinaison1.compareTo(combinaison2) > 0; // Placeholder pour la logique réelle
    }

    // Getter pour le nom du joueur
    public String getNom() {
        return nom;
    }

    // Getter pour les cartes
    public ArrayList<Carte> getCartesCachees(){
        return cartesCachees;
    }
}
