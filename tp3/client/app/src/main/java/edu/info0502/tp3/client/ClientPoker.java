package edu.info0502.tp3.client;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

import edu.info0502.tp3.shared.Message;

public class ClientPoker {
    private ObjectOutputStream outputStream;
    private String nickname;
    private static int index=0;
    
    public ClientPoker(int port, String adresseServeur){        
        connecterAuServeur(adresseServeur, port);
    }

    public void connecterAuServeur(String adresseServeur, int port){
        try(Socket socket = new Socket(adresseServeur, port);){
            outputStream = new ObjectOutputStream(socket.getOutputStream());
            
            ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());
            
            Scanner scanner = new Scanner(System.in);
            boolean isconnected = false;

            while(!isconnected){
                String default_name = "Joueur"+ClientPoker.getNextIndex();
                System.out.println("Choisissez un pseudo (défaut: "+default_name+") : ");
                System.out.print("\t");
                nickname = scanner.nextLine();
                if(nickname.isEmpty()){
                    nickname = default_name;
                }

                // Envoi
                envoyerMessage(new Message(Message.TypeMessage.AUTH, nickname));
                Message response = (Message) inputStream.readObject();
                if (response.getType() == Message.TypeMessage.AUTH_SUCC) {
                    System.out.println("Connexion réussie : " + response.getContenu());
                    isconnected = true;
                } else {
                    System.out.println("Erreur : " + response.getContenu());
                }
            }

            // Crée un thread pour gérer la réception des messages du serveur
            new Thread(() -> {
                try {
                    while (true) {
                        Message response = (Message) inputStream.readObject();
                        if (response != null) {
                            //System.out.println("Message reçu : " + response);
                            //System.out.println(response.getType());
                            if (response.getType() == Message.TypeMessage.AUTH_SUCC) {
                                System.out.println("Authentification réussie !");
                            }
                            else if (response.getType()==Message.TypeMessage.CARD_DISTRIBUTION){
                                System.out.println("Distrubution de carte :");
                            }
                            else if (response.getType()==Message.TypeMessage.FLOP){
                                System.out.println("FLOP :");
                            }
                            else if (response.getType()==Message.TypeMessage.TURN){
                                System.out.println("TURN :");
                            }
                            else if (response.getType()==Message.TypeMessage.RIVER){
                                System.out.println("RIVER :");
                            }
                            else if(response.getType()==Message.TypeMessage.RESULTS){
                                System.out.println("Résultats intermédiaires : ");
                            }
                            else if(response.getType()==Message.TypeMessage.GAME_RESULT){
                                System.out.println("Partie finie !");
                            }
                            else if(response.getType()==Message.TypeMessage.ERROR){
                                System.err.print("Erreur serveur : ");
                            }
                            else{
                                System.out.println(response.getType()+" : ");
                                System.out.println(response.getContenu());
                            }
                            System.out.println(response.getContenu());
                            System.out.println();
                        }
                    }
                } catch (IOException | ClassNotFoundException e) {
                    System.err.println("Erreur lors de la réception du message : " + e.getMessage());
                }
            }).start();

            boolean exit = false;
            while (!exit) {
                System.out.println("LANCER_PARTIE : Commencer la partie\nEXIT : Quitter la partie\n");
                String choix = scanner.nextLine();

                switch(choix){
                    case "LANCER_PARTIE":
                        envoyerMessage(new Message(Message.TypeMessage.START_POKER, null)); // Pas besoin de spécifier un contenu
                        break;
                    case "EXIT":
                        envoyerMessage(new Message(Message.TypeMessage.DISCONNECT, null)); // Pas besoin de spécifier un contenu
                        System.out.println("Déconnexion");
                        exit = true;
                        break;
                    default:
                        System.out.println("Option invalide");
                }
            }

            scanner.close();
        }

        catch(IOException e){
            System.err.println("Erreur création du thread : "+e.getMessage());
        }
        catch(ClassNotFoundException e){
            System.err.println("Erreur lors de la lecture du message : "+e.getMessage());
        }
        
    }

    public static synchronized int getNextIndex() {
        return ++index;  
    }

    private void envoyerMessage(Message message) {
        try {
            outputStream.writeObject(message);
            outputStream.flush();
        } catch (Exception e) {
            System.err.println("Erreur lors de l'envoi d'un message : " + e.getMessage());
            e.printStackTrace();
        }
    }

    public void recevoirMessage(ObjectInputStream inputStream){
        try {
            while (true) {
                if (inputStream == null) break;
                Message message = (Message) inputStream.readObject();
                switch (message.getType()) {
                    case AUTH_SUCC -> System.out.println("Connexion réussie : " + message.getContenu());
                    case CARD_DISTRIBUTION -> System.out.println("Cartes distribuées : " + message.getContenu());
                    case FLOP, TURN, RIVER -> System.out.println("Cartes visibles : " + message.getContenu());
                    case RESULTS -> System.out.println("Résultats intermédiaires : " + message.getContenu());
                    case GAME_RESULT -> System.out.println("Résultat final : " + message.getContenu());
                    case ERROR -> System.err.println("Erreur reçue du serveur : " + message.getContenu());
                    default -> System.out.println("Message non reconnu : " + message.getContenu());
                }
            }
        } catch (Exception e) {
            System.err.println("Erreur de réception d'un message : " + e.getMessage());
        }
    }

}
