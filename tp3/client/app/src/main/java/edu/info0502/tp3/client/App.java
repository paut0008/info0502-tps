package edu.info0502.tp3.client;

import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String adresse;
        String portStr;
        int port=12345;

        System.out.println("Adresse du serveur (par défaut: 10.11.17.107)");
        adresse = scanner.nextLine();
        if(adresse.equals("")){
            adresse = "10.11.17.107";
        }

        System.out.println("Port du serveur (par défaut: 12345)");
        portStr = scanner.nextLine();
        if(portStr.equals("")){
            portStr = "12345";
        }

        try {
            port = Integer.parseInt(portStr);
        } catch (NumberFormatException e) {
            System.err.println("Port incorrect, port par défaut (12345)");
        }

        System.out.println("Connexion au serveur "+adresse+" avec le port " + portStr);
        new ClientPoker(port, adresse);

        scanner.close();
    }
}
