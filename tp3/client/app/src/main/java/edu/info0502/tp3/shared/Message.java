package edu.info0502.tp3.shared;

import java.io.Serializable;

public class Message implements Serializable {

    public enum TypeMessage {
        AUTH,
        AUTH_SUCC,
        AUTH_FAIL,
        ERROR,
        START_POKER,
        DISCONNECT,
        DISCONNECT_SUCC,
        CARD_DISTRIBUTION,
        FLOP,
        TURN, 
        RIVER,
        RESULTS,
        GAME_RESULT,
        CONNEXION,
    }

    private TypeMessage type;
    private Object contenu;

    public Message(TypeMessage type, Object contenu) {
        this.type = type;
        this.contenu = contenu;
    }

    public TypeMessage getType() {
        return type;
    }

    public Object getContenu() {
        return contenu;
    }

    public void setType(TypeMessage t){
        type = t;
    }

    public void setContenu(String c){
        contenu = c;
    }

    @Override
    public String toString(){
        return "Message{type="+type+", contenu="+contenu+"}";
    }
}

