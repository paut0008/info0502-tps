package edu.info0502.tp3.shared;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;

public class Paquet implements Serializable {
    private ArrayList<Carte> cartes = new ArrayList<>();

    public Paquet() {
        for (Carte.SIGNE signe : Carte.SIGNE.values()) {
            for (int i = 1; i <= 13; i++) {
                cartes.add(new Carte(signe, i));
            }
        }
    }

    public ArrayList<Carte> getCartes() {
        return cartes;
    }

    public void melanger() {
        Collections.shuffle(cartes);
    }

    public Carte tirerCarte() {
        for (int i = 0; i < cartes.size(); i++) {
            if (cartes.get(i) != null) {
                Carte carteTiree = cartes.get(i);
                cartes.set(i, null);  // Retire la carte du paquet
                return carteTiree;
            }
        }
        return null;  // Plus de cartes à tirer
    }

    public void ajouterCarte(Carte carte) {
        for (int i = 0; i < cartes.size(); i++) {
            if (cartes.get(i) == null) {
                cartes.set(i, carte);
                break;
            }
        }
    }

    public void afficherPaquet() {
        for (Carte carte : cartes) {
            System.out.println(carte);
        }
    }
}

