package edu.info0502.tp3.shared;

import java.io.Serializable;

public class Carte implements Serializable {

    public enum SIGNE {
        Trefle,
        Pique,
        Carreau,
        Coeur
    }

    private SIGNE signe;
    private int nombre;

    public Carte(SIGNE signe, int nombre) {
        this.signe = signe;
        this.nombre = nombre;
    }

    public SIGNE getSigne() {
        return signe;
    }

    public int getNombre() {
        return nombre;
    }

    @Override
    public String toString() {
        return nombre + " de " + signe;
    }
}

