package edu.info0502.tp2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class Main {
    private ArrayList<Carte> cartes = new ArrayList<>();

    /*
     * Constructeur de la Main
     */
    public Main(Talon talon) {
        for (int i = 0; i < 5; i++) {
            cartes.add(talon.tirerCarte());
        }
    }

    /*
     * Constructeur par défaut
     */
    public Main() {
        cartes = new ArrayList<>(5); // Une main a 5 cartes
    }

    /*
     * Méthode pour afficher la main
     */
    public void afficherMain() {
        for (Carte carte : cartes) {
            System.out.println(carte);
        }
    }

    /*
     * Méthode pour trier la main par ordre croissant
     */
    private void trierMain() {
        cartes.sort(Comparator.comparingInt(Carte::getNombre));
    }

    /*
     * Méthode pour évaluer la combinaison d'une main
     */
    public String evaluerCombinaison() {
        //On trie la main
        trierMain(); 

        if (estQuinteFlushRoyale()) return "Quinte Flush Royale";
        if (estQuinteFlush()) return "Quinte Flush";
        if (estCarre()) return "Carré";
        if (estFull()) return "Full";
        if (estCouleur()) return "Couleur";
        if (estQuinte()) return "Quinte";
        if (estBrelan()) return "Brelan";
        if (estDoublePaire()) return "Double Paire";
        if (estPaire()) return "Paire";

        // Main triée : le dernier élément est le plus haut
        return "Carte la plus haute : " + cartes.get(cartes.size() - 1).getNombre() + " de " + cartes.get(cartes.size() - 1).getSigne();
    }

    /*
     * Méthode pour déterminer si la main est une quinte flush royale
     */
    private boolean estQuinteFlushRoyale() {
        return estQuinteFlush() && cartes.get(0).getNombre() == 10;
    }

    /*
     * Méthode pour déterminer si la main est une quinte flush
     */
    private boolean estQuinteFlush() {
        return estQuinte() && estCouleur();
    }

    /*
     * Méthode pour déterminer si la main est un carré (quatre cartes identiques)
     */
    private boolean estCarre() {
        int[] frequences = calculerFrequences();
        for (int frequence : frequences) {
            if (frequence == 4) return true;
        }
        return false;
    }

    /*
     * Méthode pour déterminer si la main est un full (brelan + paire)
     */
    private boolean estFull() {
        boolean brelanBool = false;
        boolean paireBool = false;
        int[] frequences = calculerFrequences();
        for (int frequence : frequences) {
            if (frequence == 3) brelanBool = true;
            if (frequence == 2) paireBool = true;
        }
        return brelanBool && paireBool;
    }

    /*
     * Méthode pour déterminer si la main est une couleur (toutes les cartes de la même couleur)
     */
    private boolean estCouleur() {
        SIGNE couleur = cartes.get(0).getSigne();
        for (Carte carte : cartes) {
            if (carte.getSigne() != couleur) return false;
        }
        return true;
    }

    /*
     * Méthode pour déterminer si la main est une quinte (5 cartes qui se suivent)
     */
    private boolean estQuinte() {
        for (int i = 0; i < cartes.size() - 1; i++) {
            if (cartes.get(i).getNombre() + 1 != cartes.get(i + 1).getNombre()) {
                return false;
            }
        }
        return true;
    }

    /*
     * Méthode pour déterminer si la main est un brelan (trois cartes identiques)
     */
    private boolean estBrelan() {
        int[] frequences = calculerFrequences();
        for (int frequence : frequences) {
            if (frequence == 3) return true;
        }
        return false;
    }

    /*
     * Méthode pour déterminer si la main a deux paires
     */
    private boolean estDoublePaire() {
        int nombreDePaires = 0;
        int[] frequences = calculerFrequences();
        for (int frequence : frequences) {
            if (frequence == 2) nombreDePaires++;
        }
        return nombreDePaires == 2;
    }

    /*
     * Méthode pour déterminer si la main a une paire
     */
    private boolean estPaire() {
        int[] frequences = calculerFrequences();
        for (int frequence : frequences) {
            if (frequence == 2) return true;
        }
        return false;
    }

    /*
     * Méthode utilitaire pour calculer la fréquence des valeurs des cartes
     */
    private int[] calculerFrequences() {
        int[] frequences = new int[14];  
        for (Carte carte : cartes) {
            frequences[carte.getNombre()]++;
        }
        return frequences;
    }

    /*
     * Méthode pour comparer deux mains
     */
    public static int comparerMains(Main main1, Main main2) {
        String combinaison1 = main1.evaluerCombinaison();
        String combinaison2 = main2.evaluerCombinaison();

        // Comparaison des combinaisons en fonction de leur force
        List<String> ordreDesCombinaisons = Arrays.asList(
            "Carte la plus haute", "Paire", "Double Paire", "Brelan", "Quinte",
            "Couleur", "Full", "Carré", "Quinte Flush", "Quinte Flush Royale"
        );

        int valeurMain1 = ordreDesCombinaisons.indexOf(combinaison1);
        int valeurMain2 = ordreDesCombinaisons.indexOf(combinaison2);

        return Integer.compare(valeurMain1, valeurMain2);
    }
}

