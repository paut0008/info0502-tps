package edu.info0502.tp2;


public class Carte {
    private SIGNE signe;
    private int nombre;

    public Carte(SIGNE signe, int nombre) {
        this.signe = signe;
        this.nombre = nombre;
    }

    public SIGNE getSigne() {
        return signe;
    }

    public int getNombre() {
        return nombre;
    }

    @Override
    public String toString() {
        return nombre + " de " + signe;
    }
}

