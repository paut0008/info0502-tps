package edu.info0502.tp2;

import java.util.ArrayList;
import java.util.List;

public class App {

    public static void testTalon() {
        // Création d'un talon avec un paquet de 52 cartes
        Talon talon = new Talon();
        System.out.println("Talon créé. Mélange du talon...");
        
        // Mélange du talon
        talon.melanger();
        
        // Tirage de 5 cartes du talon
        System.out.println("Cartes tirées du talon :");
        for (int i = 0; i < 5; i++) {
            Carte carte = talon.tirerCarte();
            System.out.println(carte.getSigne() + " " + carte.getNombre());
        }
    }

    public static void testMain() {
        // Création du talon
        Talon talon = new Talon();
        talon.melanger();
        
        // Création d'une main à partir du talon
        Main main = new Main(talon);
        
        // Affichage de la main
        System.out.println("Main du joueur :");
        main.afficherMain();
        
        // Évaluation de la combinaison de la main
        String combinaison = main.evaluerCombinaison();
        System.out.println("Combinaison évaluée : " + combinaison);
    }


    public static void testSimulationPoker() {
        // Création du talon
        Talon talon = new Talon();
        talon.melanger();
        
        // Création des mains des 4 joueurs
        ArrayList<Main> joueurs = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            joueurs.add(new Main(talon));
        }
        // Affichage des mains des joueurs et évaluation des combinaisons
        System.out.println("Mains des joueurs et combinaisons :");
        for (int i = 0; i < 4; i++) {
            System.out.println("Main du Joueur " + (i + 1) + " :");
            joueurs.get(i).afficherMain();
            System.out.println("Combinaison : " + joueurs.get(i).evaluerCombinaison());
            System.out.println();
        }
        
        // Comparaison des mains pour déterminer le vainqueur
        Main vainqueur = joueurs.get(0);
        for (int i = 1; i < 4; i++) {
            if (Main.comparerMains(vainqueur, joueurs.get(i)) < 0) {
                vainqueur = joueurs.get(i);
            }
        }
        
        // Affichage du vainqueur
        System.out.println("Le vainqueur avait comme cartes :");
        vainqueur.afficherMain();
    }

    public static void testCreationTalon() {
        Talon talon = new Talon();
        System.out.println("Test création talon:");
        System.out.println("Talon contient " + talon.getCartes().size() + " cartes.");
        assert talon.getCartes().size() == 52 : "Le talon doit contenir 52 cartes";
        talon.melanger();
        System.out.println("Talon mélangé avec succès.\n");
    }

    public static void testDistributionCartes() {
        Talon talon = new Talon();
        talon.melanger();
        List<Joueur> joueurs = new ArrayList<>();

        // Création des 4 joueurs
        for (int i = 0; i < 4; i++) {
            joueurs.add(new Joueur("Joueur " + (i + 1)));
        }

        // Distribution de 2 cartes cachées à chaque joueur
        for (Joueur joueur : joueurs) {
            joueur.ajouterCarteCachee(talon.tirerCarte());
            joueur.ajouterCarteCachee(talon.tirerCarte());
            System.out.println(joueur.getNom() + " a reçu ses 2 cartes cachées.");
        }

        // Affichage des cartes cachées de chaque joueur (pour vérification manuelle)
        for (Joueur joueur : joueurs) {
            System.out.println(joueur.getNom() + " : " + joueur.getCartesCachees());
        }
    }

    public static void testEvaluationMain() {
        Talon talon = new Talon();
        talon.melanger();
        
        // Création des cartes communes (5 cartes)
        List<Carte> cartesCommunes = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            cartesCommunes.add(talon.tirerCarte());
        }
        
        // Création d'un joueur pour tester l'évaluation
        Joueur joueur = new Joueur("TestJoueur");
        joueur.ajouterCarteCachee(talon.tirerCarte());
        joueur.ajouterCarteCachee(talon.tirerCarte());

        // Evaluation de la main avec les cartes communes
        joueur.evaluerMain(cartesCommunes);
        System.out.println("Évaluation de la main de " + joueur.getNom() + ": " + joueur.getEvaluationMain());

        // Affichage des cartes communes et la main du joueur pour vérification
        System.out.println("Cartes communes: ");
        for (Carte carte : cartesCommunes) {
            System.out.println(carte);
        }
        System.out.println("Main évaluée de " + joueur.getNom() + ": " + joueur.getEvaluationMain() + "\n");
    }



    public static void main(String[] args) {
        // Appel des fonctions de test
        testTalon();
        System.out.println("---------------------");
        testMain();
        System.out.println("---------------------");
        testSimulationPoker();

        System.out.println();
        System.out.println("---------------------");
        System.out.println();
        testCreationTalon();
        testDistributionCartes();
        testEvaluationMain();
    }
}
