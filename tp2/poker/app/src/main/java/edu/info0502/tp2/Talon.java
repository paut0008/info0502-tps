package edu.info0502.tp2;

import java.util.ArrayList;
import java.util.Collections;

public class Talon {
    private ArrayList<Carte> cartes = new ArrayList<>();

    public Talon() {
        Paquet paquet = new Paquet();
        Collections.addAll(cartes, paquet.getCartes());
    }

    public ArrayList<Carte> getCartes() {
        return cartes;
    }

    public void melanger() {
        Collections.shuffle(cartes);
    }

    public Carte tirerCarte() {
        if (!cartes.isEmpty()) {
            return cartes.remove(0); 
        }
        return null; 
    }
}
