package edu.info0502.tp2;

import java.util.ArrayList;
import java.util.Collections;

public class Paquet {
    private Carte[] cartes = new Carte[52];

    public Paquet() {
        int index = 0;
        for (SIGNE signe : SIGNE.values()) {
            for (int i = 1; i <= 13; i++) {
                cartes[index++] = new Carte(signe, i);
            }
        }
    }

    public Carte[] getCartes() {
        return cartes;
    }

    public void melanger() {
        ArrayList<Carte> cartesList = new ArrayList<>();
        Collections.addAll(cartesList, cartes);
        Collections.shuffle(cartesList);
        cartes = cartesList.toArray(new Carte[0]);
    }

    public Carte tirerCarte() {
        for (int i = 0; i < cartes.length; i++) {
            if (cartes[i] != null) {
                Carte carteTiree = cartes[i];
                cartes[i] = null;  // Retire la carte du paquet
                return carteTiree;
            }
        }
        return null;  // Plus de cartes à tirer
    }

    public void ajouterCarte(Carte carte) {
        for (int i = 0; i < cartes.length; i++) {
            if (cartes[i] == null) {
                cartes[i] = carte;
                break;
            }
        }
    }
}

