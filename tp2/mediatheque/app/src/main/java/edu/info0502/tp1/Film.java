package edu.info0502.tp1;

public class Film extends Media {
    private String realisateur;
    private int annee;

    public Film(String titre, StringBuffer cote, int note, String r, int a){
        super(titre, cote, note);
        realisateur = r;
        annee = a;
    }

    public Film(){
        this("", new StringBuffer(), 0, "", 0);
    }

    public Film(Film f){
        this(f.getTitre(), new StringBuffer(f.getCote()), f.getNote(), f.getRealisateur(), f.getAnnee());
    }

    public String getRealisateur() { return realisateur; }
    public int getAnnee() { return annee; }

    public void setRealisateur(String r) { realisateur = r; }
    public void setAnnee(int a) { annee = a; }

    
    @Override
    public Film clone(){
        return new Film(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Media media = (Media) obj;
        return getNote() == media.getNote() && titre.equals(media.titre) && cote.equals(media.cote);
    }

    @Override
    public String toString() {
        return super.toString() + ", Realisateur: " + realisateur + ", Année: " + annee + "";
    }
} 
