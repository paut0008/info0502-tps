package edu.info0502.tp1;

import java.util.Vector;

public class Test {
    public static void main(String[] args) {

        // Toutes les médias créés auront pour nom de médiathèque "Médiathèque Municipale"
        Media.setNom("Médiathèque Municipale");

        Mediatheque mediatheque = new Mediatheque();
        mediatheque.setProprietaire("Alice");

        Film f1 = new Film("Inception", new StringBuffer("1234"), 9, "Christopher Nolan", 2010);
        Film f2 = new Film("The Dark Knight", new StringBuffer("5678"), 10, "Christopher Nolan", 2008);
        mediatheque.add(f1);
        mediatheque.add(f2);

        Livre l1 = new Livre("1984", new StringBuffer("4321"), 8, "George Orwell", "Dystopie", 328);
        mediatheque.add(l1);

        System.out.println(mediatheque);

        Mediatheque mediathequeCopie = new Mediatheque(mediatheque);
        System.out.println("\nCopie de la médiathèque : ");
        System.out.println(mediathequeCopie);
    }
}
