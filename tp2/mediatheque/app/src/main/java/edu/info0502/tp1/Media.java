package edu.info0502.tp1;

public class Media implements Cloneable {
    // Données d'instance
    protected String titre;
    protected StringBuffer cote;
    private int note; 

    // Données de classe
    private static String nom;

    // Constructeur par initialisation
    public Media(String titre, StringBuffer cote, int note){
        this.titre = titre;
        this.cote = cote;
        this.note = note;
    }

    // Constructeur par défaut
    public Media(){
        this("", new StringBuffer(), 0);
    }

    // Constructeur par copie
    public Media(Media m){
        this(m.getTitre(), new StringBuffer(m.getCote()), m.getNote());
    }

    // Getters et setters des données d'instance
    public String getTitre(){ return titre; }
    public StringBuffer getCote(){ return cote; }
    public int getNote(){ return note; }

    public void setTitre(String titre){ this.titre = titre; }
    public void setCote(StringBuffer cote){ this.cote = cote; }
    public void setNote(int note){ this.note = note; }

    // Getters et setters des données de classe
    public static String getNom(){ return nom; }
    public static void setNom(String n){ nom = n; }

    // Masquage des méthodes
    @Override
    public Media clone(){
        try {
            return (Media)super.clone();
        }
        catch(CloneNotSupportedException e){
            System.out.println("Error : " + e);
            return null;
        }
    }

    @Override
    public boolean equals(Object obj){
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Media m = (Media)(obj);
        return (this.titre == m.titre && this.cote.equals(m.cote) && this.note == m.note);
    }

    @Override
    public String toString(){
        return "Titre: "+ titre + ", Cote: " + cote + ", Note: " + note + ", Nom Médiathèque: " + nom;
    }
}