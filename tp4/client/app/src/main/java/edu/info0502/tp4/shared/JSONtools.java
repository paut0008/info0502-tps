package edu.info0502.tp4.shared;

import org.json.JSONObject;
import org.json.JSONArray;

public class JSONtools {
    public static JSONObject USER_REGISTRATION_REQUEST(
        String email,
        String name
    ){
        JSONObject request = new JSONObject();
        JSONObject data = new JSONObject();
        request.put("type", "USER_REGISTRATION");
        data.put("email", email);
        data.put("name", name);
        request.put("data", data);

        return request;
    }

    public static JSONObject START_QCM(
        int qcm_id,
        int user_id,
        String email
    ){
        JSONObject request = new JSONObject();
        JSONObject data = new JSONObject();
        request.put("type", "START_QCM");
        data.put("qcm_id", qcm_id);
        data.put("user_id", user_id);
        data.put("email", email);
        request.put("data", data);

        return request;
    }

    /*
    {
      "type": "SUBMIT_QCM",
      "qcm_id": 0
      "user_id": 123,
      "data": 
        "qcm":{
          [
            {
              "id": 1,
              "title_question": "Quelle est la capitale de la France ?",
              "answers": [
                { "id": 1, "answer": "Paris", "chosen": false},
                { "id": 2, "answer": "Londres", "chosen": true},
                { "id": 3, "answer": "Berlin", "chosen": false}
              ]
              // etc.
            }
          ]
        }
      }
    }
     */
    // On a un nouveau tableau QCM, on l'ajoute au QCM existant.
    public static JSONObject SUBMIT_QCM(
        int qcm_id,
        int user_id,
        JSONArray qcm,
        String email
    ){
        JSONObject request = new JSONObject();
        JSONObject data = new JSONObject();
        request.put("type", "SUBMIT_QCM");
        data.put("qcm_id", qcm_id);
        data.put("user_id", user_id);
        data.put("email", email);
        data.put("qcm", qcm);
        request.put("data", data);

        return request;
    }
}
