package edu.info0502.tp4.client;

import com.rabbitmq.client.*;

import edu.info0502.tp4.shared.JSONtools;

import org.json.JSONObject;
import org.json.JSONArray;

import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeoutException;

public class App {

    private final static String HOST = "10.11.17.107";
    private final static String USERNAME = "admin";
    private final static String PASSWORD = "admin";


    private final static String REGISTRATION_QUEUE = "user.registration";
    private final static String REGISTRATION_RESPONSE_QUEUE = "user.registration.response";

    private final static String QCM_REQUEST = "qcm.request";
    private final static String QCM_RESPONSE = "qcm.response";

    private final static String QCM_CORRECTION = "user.correction"; 
    private final static String QCM_CORRECTION_RESPONSE = "user.correction.response"; 

    public static void main(String[] args) throws IOException, TimeoutException, InterruptedException {
        // Se connecter au serveur RabbitMQ
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(HOST);
        factory.setUsername(USERNAME);
        factory.setPassword(PASSWORD);
        Scanner scanner = new Scanner(System.in);
        try (Connection connection = factory.newConnection();
            Channel channel = connection.createChannel()) {

            // Déclarer les files nécessaires
            channel.queueDeclare(REGISTRATION_QUEUE,          true, false, false, null);
            channel.queueDeclare(REGISTRATION_RESPONSE_QUEUE, true, false, false, null);
            channel.queueDeclare(QCM_REQUEST,                 true, false, false, null);
            channel.queueDeclare(QCM_RESPONSE,                true, false, false, null);
            channel.queueDeclare(QCM_CORRECTION,              true, false, false, null);
            channel.queueDeclare(QCM_CORRECTION_RESPONSE,     true, false, false, null);

            // Demander à l'utilisateur de saisir ses informations
            
            System.out.print("Entrez votre email: ");
            String email = scanner.nextLine();
            System.out.print("Entrez votre nom: ");
            String name = scanner.nextLine();

            // Créer la requête d'inscription au format JSON
            JSONObject request = new JSONObject();
            request.put("type", "USER_REGISTRATION");

            JSONObject data = new JSONObject();
            data.put("email", email);
            data.put("name", name);
            request.put("data", data);

            String message = request.toString();
            channel.basicPublish("", REGISTRATION_QUEUE, null, message.getBytes());
            //System.out.println("Requête d'inscription envoyée : " + message);

            // Utiliser un CountDownLatch pour attendre la fin des opérations
            CountDownLatch latch = new CountDownLatch(1);

            // Consommer la réponse du serveur d'inscription
            Consumer registrationResponseConsumer = new DefaultConsumer(channel) {
                @Override
                public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                    String responseMessage = new String(body, "UTF-8");
                    //System.out.println("Réponse d'inscription reçue : " + responseMessage);

                    JSONObject response = new JSONObject(responseMessage);
                    if (response.getString("type").equals("USER_REGISTRATION_RESPONSE")) {
                        JSONObject responseData = response.getJSONObject("data");
                        int userId = responseData.getInt("id");
                        String userEmail = responseData.getString("email");
                        System.out.println("L'utilisateur a été inscrit avec succès ! ID : " + userId);

                        // Choix du QCM
                        System.out.println("Choisissez un QCM (qcm de test : 0):");
                        String qcmChoice = scanner.nextLine();
                        int qcmId;
                        if(qcmChoice.equals("") || qcmChoice.equals("0")) {
                            qcmId = 0;
                        } else {
                            qcmId = Integer.parseInt(qcmChoice);
                        }

                        // Envoyer une requête de QCM après l'inscription réussie
                        JSONObject qcmRequest = JSONtools.START_QCM(qcmId, userId, userEmail);

                        channel.basicPublish("", QCM_REQUEST, null, qcmRequest.toString().getBytes());
                        System.out.println("Requête de QCM envoyée.");
                    }
                }
            };
            channel.basicConsume(REGISTRATION_RESPONSE_QUEUE, true, registrationResponseConsumer);

            // Consommation de la réponse de la requête de QCM
            Consumer qcmResponseConsumer = new DefaultConsumer(channel) {
                @Override
                public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                    String responseMessage = new String(body, "UTF-8");
                    //System.out.println("Réponse de la requête START_QCM reçue : " + responseMessage);

                    JSONObject response = new JSONObject(responseMessage);
                    if (response.getString("type").equals("QCM_STARTED")) {
                        JSONObject responseData = response.getJSONObject("data");
                        String userEmail = responseData.getString("email");
                        //System.out.println("QCM reçu : "+responseData.toString());

                        // Gestion des questions avec l'objet JSON reçu
                        JSONArray qcmArray = responseData.getJSONArray("qcm");
                        for (int i = 0; i < qcmArray.length(); i++) {
                            JSONObject qcm = qcmArray.getJSONObject(i);
                            JSONArray questions = qcm.getJSONArray("questions");
                            for (int j = 0; j < questions.length(); j++) {
                                JSONObject question = questions.getJSONObject(j);
                                System.out.println("Question : " + question.getString("title_question"));
                                JSONArray answers = question.getJSONArray("answers");
                                for (int k = 0; k < answers.length(); k++) {
                                    JSONObject answer = answers.getJSONObject(k);
                                    System.out.println((k + 1) + ". " + answer.getString("answer"));
                                }
                                System.out.print("Votre réponse : ");
                                String answerChoice = scanner.nextLine();

                                // Mettre à jour les clés "chosen" pour chaque proposition
                                for (int k = 0; k < answers.length(); k++) {
                                    JSONObject answer = answers.getJSONObject(k);
                                    if (answerChoice.equals(String.valueOf(k + 1))) {
                                        answer.put("chosen", true);
                                    } else {
                                        answer.put("chosen", false);
                                    }
                                }
                            }

                            // Envoyer les réponses mises à jour dans la file QCM_CORRECTION
                            JSONObject qcmCorrection = JSONtools.SUBMIT_QCM(qcm.getInt("id"), responseData.getInt("user_id"), questions, userEmail);

                            channel.basicPublish("", QCM_CORRECTION, null, qcmCorrection.toString().getBytes());
                            //System.out.println("Réponses envoyées pour correction : " + qcmCorrection.toString());
                        }
                    }
                }
            };
            channel.basicConsume(QCM_RESPONSE, true, qcmResponseConsumer);

            // Consommer les corrections de QCM
            Consumer correctionRequestConsumer = new DefaultConsumer(channel) {
                @Override
                public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                    String responseMessage = new String(body, "UTF-8");

                    JSONObject response = new JSONObject(responseMessage);
                    if (response.getString("type").equals("QCM_CORRECTION")) {
                        //System.out.println("Correction reçue : " + response.toString());

                        JSONObject responseData = response.getJSONObject("data");
                        JSONArray questions = responseData.getJSONArray("qcm");

                        // Affichage de la correction
                        // Si la réponse est correcte, on affiche "Correct" sinon on affiche "Incorrect"
                        // On vérifie donc la validité entre "chosen" et "is_correct"
                        for (int i = 0; i < questions.length(); i++) {
                            JSONObject question = questions.getJSONObject(i);
                            System.out.println("Question : " + question.getString("title_question"));
                            JSONArray answers = question.getJSONArray("answers");
                            for (int j = 0; j < answers.length(); j++) {
                                JSONObject answer = answers.getJSONObject(j);
                                if (answer.getBoolean("is_correct")) {
                                    System.out.println("Réponse : " + answer.getString("answer"));
                                    if (answer.getBoolean("chosen")) {
                                        System.out.println("Correct");
                                    } else {
                                        System.out.println("Incorrect");
                                    }
                                }
                            }
                            System.out.println();
                        }

                        // Décrémenter le CountDownLatch pour indiquer que l'opération est terminée
                        latch.countDown();
                    }
                }
            };
            channel.basicConsume(QCM_CORRECTION_RESPONSE, true, correctionRequestConsumer);

            // Attendre que toutes les opérations soient terminées avant de fermer le canal
            latch.await();
            scanner.close();
        }
    }
}
