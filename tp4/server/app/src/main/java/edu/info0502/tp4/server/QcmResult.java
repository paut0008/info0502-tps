package edu.info0502.tp4.server;

public class QcmResult {
    private int qcmId;
    private String qcmTitle;
    private int score;
    private int totalQuestions;

    public QcmResult(int qcmId, String qcmTitle, int score, int totalQuestions) {
        this.qcmId = qcmId;
        this.qcmTitle = qcmTitle;
        this.score = score;
        this.totalQuestions = totalQuestions;
    }

    // Getters
    public int getQcmId() { return qcmId; }
    public String getQcmTitle() { return qcmTitle; }
    public int getScore() { return score; }
    public int getTotalQuestions() { return totalQuestions; }

    @Override
    public String toString() {
        return "QcmResult{qcmId=" + qcmId + ", title='" + qcmTitle + "', score=" + score + "/" + totalQuestions + "}";
    }
}
