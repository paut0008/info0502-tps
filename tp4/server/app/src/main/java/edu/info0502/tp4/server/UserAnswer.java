package edu.info0502.tp4.server;

public class UserAnswer {
    private int questionId;
    private String userChoice;

    public UserAnswer(int questionId, String userChoice) {
        this.questionId = questionId;
        this.userChoice = userChoice;
    }

    public int getQuestionId() {
        return questionId;
    }

    public String getUserChoice() {
        return userChoice;
    }
}
