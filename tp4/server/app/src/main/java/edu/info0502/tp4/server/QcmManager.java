package edu.info0502.tp4.server;

import com.rabbitmq.client.Connection;  
import com.rabbitmq.client.Channel;     
import com.rabbitmq.client.GetResponse; 
import com.rabbitmq.client.ConnectionFactory; 

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeoutException;

public class QcmManager {
    private ArrayList<Qcm> qcms;
    private final static String QUESTIONS_QUEUE = "user.question";
    private final static String ANSWERS_QUEUE = "user.answer";

    public QcmManager() {
        this.qcms = new ArrayList<>();
    }

    public ArrayList<Qcm> getAllQcms() {
        return qcms;
    }

    // Retourne un QCM par son nom ou son ID
    public Qcm getQcmById(int qcmId) {
        // Logique pour récupérer un QCM
        for (Qcm qcm : qcms) {
            if (qcm.getId() == qcmId) {
                return qcm;
            }
        }
        return null;  // Retourner null si le QCM n'est pas trouvé
    }

    // Créer un nouveau QCM
    public Qcm createQcm(String title, Qcm qcm) {
        qcms.add(qcm);
        return qcm;
    }

    // Gérer la session du QCM
    public void handleQcmSession(Qcm qcm, User user) throws IOException, TimeoutException {
        // Connexion RabbitMQ
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        try (Connection connection = factory.newConnection(); 
             Channel channel = connection.createChannel()) {

            // Déclaration des files RabbitMQ
            channel.queueDeclare(QUESTIONS_QUEUE, false, false, false, null);
            channel.queueDeclare(ANSWERS_QUEUE, false, false, false, null);

            // Envoi des questions au client
            for (Question question : qcm.getQuestions()) {
                String message = createQuestionMessage(question);
                channel.basicPublish("", QUESTIONS_QUEUE, null, message.getBytes());
            }

            // Recevoir les réponses du client et les corriger
            ArrayList<UserAnswer> userAnswers = receiveUserAnswers(channel);

            // Corriger les réponses et obtenir le résultat
            QcmResult result = correctAnswers(qcm, userAnswers);

            // Afficher le résultat (ou sauvegarder)
            System.out.println("Résultat du QCM: " + result);
        }
    }

    private String createQuestionMessage(Question question) {
        // Créer un message pour la question
        StringBuilder message = new StringBuilder();
        message.append("Question: ").append(question.getTitle()).append("\n");
        for (Proposition prop : question.getPropositions()) {
            message.append(prop.getId()).append(": ").append(prop.getText()).append("\n");
        }
        return message.toString();
    }

    private ArrayList<UserAnswer> receiveUserAnswers(Channel channel) throws IOException {
        ArrayList<UserAnswer> answers = new ArrayList<>();
        
        while (answers.size() < qcms.get(0).getQuestions().size()) {
            GetResponse response = channel.basicGet(ANSWERS_QUEUE, true);
            if (response != null) {
                // Extraire le message
                String userAnswerMessage = new String(response.getBody(), "UTF-8");
    
                // Supposons que le message est sous la forme "questionId:answer"
                String[] parts = userAnswerMessage.split(":");
                if (parts.length == 2) {
                    int questionId = Integer.parseInt(parts[0].trim());
                    String answer = parts[1].trim();
    
                    // Ajouter la réponse à la liste
                    answers.add(new UserAnswer(questionId, answer));
                }
            }
        }
        return answers;
    }

    private QcmResult correctAnswers(Qcm qcm, ArrayList<UserAnswer> userAnswers) {
        int correctAnswers = 0;
        for (UserAnswer answer : userAnswers) {
            Question question = qcm.getQuestionById(answer.getQuestionId());
            if (question != null && question.isCorrect(answer.getUserChoice())) {
                correctAnswers++;
            }
        }

        return new QcmResult(qcm.getId(), qcm.getTitle(), correctAnswers, qcm.getQuestions().size());
    }
}
