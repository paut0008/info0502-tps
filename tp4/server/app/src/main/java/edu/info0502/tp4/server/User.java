package edu.info0502.tp4.server;

import java.util.ArrayList;

public class User {
    private int id;
    private String name;
    private String email;
    private ArrayList<QcmResult> results;

    public User(int id, String name, String email) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.results = new ArrayList<>();
    }

    // Getters
    public int getId() { return id; }
    public String getName() { return name; }
    public String getEmail() { return email; }
    public ArrayList<QcmResult> getResults() { return results; }

    // Ajouter un résultat
    public void addResult(QcmResult result) {
        results.add(result);
    }

    @Override
    public String toString() {
        return "User{id=" + id + ", name='" + name + "', email='" + email + "', results=" + results + "}";
    }
}
