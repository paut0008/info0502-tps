package edu.info0502.tp4.server;

import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;

public class Qcm {
    private int id;
    private String title;
    private ArrayList<Question> questions;

    public Qcm(int id, String title) {
        if(id==0){
            new Qcm();
        }
        else{
            this.id = id;
            this.title = title;
            this.questions = new ArrayList<>();
        }
    }

    public Qcm(){
        this.id = 0;
        this.title = "QCM de Test";
        this.questions = new ArrayList<>();

        ArrayList<Proposition> question1Propositions = new ArrayList<>();
        question1Propositions.add(new Proposition(1, "public"));
        question1Propositions.add(new Proposition(2, "private"));
        question1Propositions.add(new Proposition(3, "package-private", true));
        question1Propositions.add(new Proposition(4, "protected"));
    
        this.questions.add(new Question(1, "Quelle est la portée par défaut des variables dans une classe ?", question1Propositions));
    
        ArrayList<Proposition> question2Propositions = new ArrayList<>();
        question2Propositions.add(new Proposition(1, "run()"));
        question2Propositions.add(new Proposition(2, "execute()"));
        question2Propositions.add(new Proposition(3, "start()", true));
        question2Propositions.add(new Proposition(4, "begin()"));
    
        this.questions.add(new Question(2, "Quelle est la méthode pour démarrer un thread ?", question2Propositions));
    }

    public void addQuestion(Question question) {
        questions.add(question);
    }

    public int getId() { return id; }
    public String getTitle() { return title; }
    public ArrayList<Question> getQuestions() { return questions; }

    public JSONObject toJson() {
        JSONObject json = new JSONObject();
        json.put("id", id);
        json.put("title", title);

        JSONArray questionsArray = new JSONArray();
        for (Question question : questions) {
            questionsArray.put(question.toJson());
        }
        json.put("questions", questionsArray);

        return json;
    }

    public Question getQuestionById(int questionId) {
        for (Question question : questions) {
            if (question.getId() == questionId) {
                return question;
            }
        }
        return null;
    }    

    @Override
    public String toString() {
        return "QCM{id=" + id + ", title='" + title + "', questions=" + questions + "}";
    }
}
