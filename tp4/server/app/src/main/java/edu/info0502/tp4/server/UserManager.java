package edu.info0502.tp4.server;

import java.util.ArrayList;

public class UserManager {
    private ArrayList<User> users;
    private int nextId;

    public UserManager() {
        this.users = new ArrayList<>();
        this.nextId = 1;
    }

    // Create a new user
    public User createUser(String name, String email) {
        User newUser = new User(nextId++, name, email);
        users.add(newUser);
        return newUser;
    }

    // Delete a user
    public boolean deleteUser(int userId) {
        return users.removeIf(user -> user.getId() == userId);
    }

    // Get a user by ID
    public User getUser(int userId) {
        return users.stream()
                    .filter(user -> user.getId() == userId)
                    .findFirst()
                    .orElse(null);
    }

    // Add a QCM result to a user
    public boolean addQcmResult(int userId, QcmResult result) {
        User user = getUser(userId);
        if (user != null) {
            user.addResult(result);
            return true;
        }
        return false;
    }

    // Get the QCM results of a user
    public ArrayList<QcmResult> getUserResults(int userId) {
        User user = getUser(userId);
        return user != null ? user.getResults() : null;
    }

    // ArrayList all users (debug or admin purposes)
    public void listAllUsers() {
        users.forEach(System.out::println);
    }
}
