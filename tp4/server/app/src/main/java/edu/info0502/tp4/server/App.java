package edu.info0502.tp4.server;

import edu.info0502.tp4.shared.JSONtools;

import com.rabbitmq.client.*;
import org.json.JSONObject;
import org.json.JSONArray;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeoutException;

public class App {

    private final static String REGISTRATION_QUEUE = "user.registration";
    private final static String REGISTRATION_RESPONSE_QUEUE = "user.registration.response";

    private final static String QCM_REQUEST = "qcm.request";
    private final static String QCM_RESPONSE = "qcm.response";

    private final static String QCM_CORRECTION = "user.correction"; 
    private final static String QCM_CORRECTION_RESPONSE = "user.correction.response"; 


    private static int userId = 0;

    public static void main(String[] args) throws IOException, TimeoutException {
        System.out.println("Démarrage du serveur QCM...");
        
        QcmManager qcmManager = new QcmManager();
        Qcm qcm_test = new Qcm();
        qcmManager.createQcm(qcm_test.getTitle(), qcm_test);

        // Lancer le serveur RabbitMQ dans une boucle infinie pour traiter les messages
        runServer(qcmManager);
    }

    private static void runServer(QcmManager qcmManager) throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        try (Connection connection = factory.newConnection();
            Channel channel = connection.createChannel()) {

            channel.queueDeclare(REGISTRATION_QUEUE,          true, false, false, null);
            channel.queueDeclare(REGISTRATION_RESPONSE_QUEUE, true, false, false, null);
            channel.queueDeclare(QCM_REQUEST,                 true, false, false, null);
            channel.queueDeclare(QCM_RESPONSE,                true, false, false, null);
            channel.queueDeclare(QCM_CORRECTION,              true, false, false, null);
            channel.queueDeclare(QCM_CORRECTION_RESPONSE,     true, false, false, null);

            System.out.println("Serveur en écoute. Ctrl+C pour arrêter.");

            CountDownLatch latch = new CountDownLatch(1);

            // Consommer la requête du client d'inscription
            Consumer registrationRequestConsumer = new DefaultConsumer(channel) {
                @Override
                public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                    String requestMessage = new String(body, "UTF-8");
                    System.out.println("\nRequête d'inscription reçue : " + requestMessage+"\n");

                    JSONObject request = new JSONObject(requestMessage);
                    if (request.getString("type").equals("USER_REGISTRATION")) {
                        JSONObject requestData = request.getJSONObject("data");
                        String userEmail = requestData.getString("email");
                        String userName = requestData.getString("name");
                        System.out.println("L'utilisateur" + userName + " (" + userEmail + ") souhaite s'inscrire.");

                        // Envoyer une requête de QCM après l'inscription réussie
                        JSONObject qcmRequest = JSONtools.USER_REGISTRATION_RESPONSE(registerUser(userEmail, userName), userEmail);
                        //System.out.println(qcmRequest.toString());
                        
                        channel.basicPublish("", REGISTRATION_RESPONSE_QUEUE, null, qcmRequest.toString().getBytes());
                        System.out.println("L'utilisateur " + userName + " (" + userEmail + ") est inscrit.");
                    }

                    // // Décrémenter le CountDownLatch pour indiquer que l'opération est terminée
                    // latch.countDown();
                }
            };
            channel.basicConsume(REGISTRATION_QUEUE, true, registrationRequestConsumer);

            // Consommer la requête du client d'inscription
            Consumer qcmRequestConsumer = new DefaultConsumer(channel) {
                @Override
                public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                    String requestMessage = new String(body, "UTF-8");
                    System.out.println("\nRequête de QCM reçue : " + requestMessage+"\n");

                    JSONObject request = new JSONObject(requestMessage);
                    if (request.getString("type").equals("START_QCM")) {
                        JSONObject requestData = request.getJSONObject("data");
                        int qcmId = requestData.getInt("qcm_id");
                        int userId = requestData.getInt("user_id");
                        String userEmail = requestData.getString("email");
                        System.out.println("L'utilisateur " + userId + " souhaite effectuer le QCM "+qcmId);

                        // Récupération du QCM
                        Qcm qcm = qcmManager.getQcmById(qcmId);
                        
                        // Envoyer une requête de QCM après l'inscription réussie
                        JSONObject qcmRequest = JSONtools.QCM_STARTED(qcmId, userId, userEmail, qcm);

                        channel.basicPublish("", QCM_RESPONSE, null, qcmRequest.toString().getBytes());
                        System.out.println("QCM " + qcmId + " envoyé à user "+userId);
                    }

                    // // Décrémenter le CountDownLatch pour indiquer que l'opération est terminée
                    // latch.countDown();
                }
            };
            channel.basicConsume(QCM_REQUEST, true, qcmRequestConsumer);
            //channel.basicConsume(REGISTRATION_QUEUE, true, registrationRequestConsumer);

            // Consommer la requête de correction du client
            Consumer correctionRequestConsumer = new DefaultConsumer(channel) {
                @Override
                public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                    String requestMessage = new String(body, "UTF-8");
                    System.out.println("\nRequête de correction de QCM reçue : " + requestMessage+"\n");

                    JSONObject request = new JSONObject(requestMessage);
                    if (request.getString("type").equals("SUBMIT_QCM")) {
                        JSONObject requestData = request.getJSONObject("data");
                        int userId = requestData.getInt("user_id");
                        int qcmId = requestData.getInt("qcm_id");
                        System.out.println("L'utilisateur " + userId + " souhaite avoir la correction du QCM " + qcmId);

                        // Récupération de la correction du QCM
                        Qcm qcmCorr = qcmManager.getQcmById(qcmId);
                        if (qcmCorr == null) {
                            System.err.println("QCM introuvable pour l'ID : " + qcmId);
                            JSONObject errorResponse = new JSONObject();
                            errorResponse.put("type", "ERROR");
                            errorResponse.put("message", "QCM introuvable pour l'ID : " + qcmId);
                            channel.basicPublish("", QCM_CORRECTION_RESPONSE, null, errorResponse.toString().getBytes(StandardCharsets.UTF_8));
                            return;
                        }

                        JSONArray questionsArray = requestData.getJSONArray("qcm");

                        int correctAnswers = 0;
                        int totalQuestions = questionsArray.length();

                        // On place les valeurs "is_correct" trouvées dans qcmCorr dans les propositions du JSONArray récupéré (avec une nouvelle clé)
                        for (int i_quest = 0; i_quest < questionsArray.length(); i_quest++) {
                            JSONObject questionJSON = questionsArray.getJSONObject(i_quest);
                            JSONArray answers = questionJSON.getJSONArray("answers");
                            for (int i_prop = 0; i_prop < answers.length(); i_prop++) {
                                JSONObject propJSON = answers.getJSONObject(i_prop);
                                boolean isCorrect = qcmCorr.getQuestions().get(i_quest).getPropositions().get(i_prop).isCorrect();
                                propJSON.put("is_correct", isCorrect);

                                // Vérifier si la réponse choisie est correcte
                                if (propJSON.getBoolean("chosen") && isCorrect) {
                                    correctAnswers++;
                                }
                            }
                        }

                        int incorrectAnswers = totalQuestions - correctAnswers;

                        JSONObject qcmResponse = JSONtools.QCM_CORRECTION(correctAnswers, totalQuestions, correctAnswers, incorrectAnswers, questionsArray);

                        channel.basicPublish("", QCM_CORRECTION_RESPONSE, null, qcmResponse.toString().getBytes(StandardCharsets.UTF_8));
                        System.out.println("\nCorrection du QCM envoyée au client :\n"+qcmResponse.toString()+"\n");

                        // Mettre à jour les informations dans le fichier history.json
                        String email = requestData.getString("email");
                        JSONtools.update_history(email, qcmId, correctAnswers, totalQuestions);

                        // Afficher l'historique des utilisateurs
                        JSONtools.print_history();
                    }

                    // Décrémenter le CountDownLatch pour indiquer que l'opération est terminée
                    latch.countDown();
                }
            };
            channel.basicConsume(QCM_CORRECTION, true, correctionRequestConsumer);
            
            // Boucle infinie pour garder le serveur en marche
            while (true) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                    break;
                }
            }
        }
    }



    private static int registerUser(String email, String name) {
        return userId++;
    }
}
