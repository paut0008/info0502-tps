package edu.info0502.tp4.shared;

import edu.info0502.tp4.server.Qcm;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import org.json.JSONObject;
import org.json.JSONArray;

public class JSONtools {
    public static JSONObject USER_REGISTRATION_REQUEST(
        String email,
        String name
    ){
        JSONObject request = new JSONObject();
        JSONObject data = new JSONObject();
        request.put("type", "USER_REGISTRATION");
        data.put("email", email);
        data.put("name", name);
        request.put("data", data);

        return request;
    }

    public static JSONObject USER_REGISTRATION_RESPONSE(
        int id,
        String email
    ){
        JSONObject response = new JSONObject();
        JSONObject data = new JSONObject();
        response.put("type", "USER_REGISTRATION_RESPONSE");
        data.put("id", id);
        data.put("email", email);

        response.put("data", data);
        
        return response;
    }

    public static JSONObject START_QCM(
        int qcm_id,
        int user_id
    ){
        JSONObject request = new JSONObject();
        JSONObject data = new JSONObject();
        request.put("type", "START_QCM");
        data.put("qcm_id", qcm_id);
        data.put("user_id", user_id);
        request.put("data", data);

        return request;
    }

    public static JSONObject QCM_STARTED(
        int qcm_id,
        int user_id,
        String email,
        Qcm qcm
    ){
        JSONObject response = new JSONObject();
        JSONObject data = new JSONObject();
        JSONArray qcm_json = new JSONArray();
        
        response.put("type", "QCM_STARTED");
        data.put("qcm_id", qcm_id);
        data.put("user_id", user_id);
        data.put("email", email);

        qcm_json.put(qcm.toJson());
        data.put("qcm", qcm_json);

        response.put("data", data);

        return response;
    }

    /*
    {
      "type": "SUBMIT_QCM",
      "qcm_id": 0
      "user_id": 123,
      "data": 
        "qcm":{
          [
            {
              "id": 1,
              "title_question": "Quelle est la capitale de la France ?",
              "answers": [
                { "id": 1, "answer": "Paris", "chosen": false},
                { "id": 2, "answer": "Londres", "chosen": true},
                { "id": 3, "answer": "Berlin", "chosen": false}
              ]
              // etc.
            }
          ]
        }
      }
    }
     */
    // On va parcourir qcm et ajouter à chaque éléments answers une clé chosen qui dépendra en fonction du choix (choices)
    public static JSONObject SUBMIT_QCM(
        int qcm_id,
        int user_id,
        String email,
        JSONArray qcm,
        boolean[][] choices
    ){
        JSONObject request = new JSONObject();
        JSONObject data = new JSONObject();
        request.put("type", "SUBMIT_QCM");
        data.put("qcm_id", qcm_id);
        data.put("user_id", user_id);
        data.put("email", email);
    
        JSONArray qcm_json = new JSONArray();
        for (int i = 0; i < qcm.length(); i++) {
            JSONObject question = qcm.getJSONObject(i);
            JSONArray answers = question.getJSONArray("answers");
            for (int j = 0; j < answers.length(); j++) {
                JSONObject answer = answers.getJSONObject(j);
                answer.put("chosen", choices[i][j]);
            }
            qcm_json.put(question);
        }

        data.put("qcm", qcm_json);
        request.put("data", data);

        return request;
    }
    
    /*
    {
      "type": "QCM_CORRECTION",
      "score": 9,
      "total_questions": 10,
      "correct_answers": 9,
      "incorrect_answers": 1,
      "data": {
        "qcm": [
          {
            "id": 1,
            "title_question": "Quelle est la capitale de la France ?",
            "answers": [
              { "id": 1, "answers": "Paris", "chosen": false, "is_correct": true },
              { "id": 2, "answers": "Londres", "chosen": true, "is_correct": false },
              { "id": 3, "answers": "Berlin", "chosen": false, "is_correct": false }
            ]
          },
          // etc.
        ]
      }
    }
     */
    public static JSONObject QCM_CORRECTION(
        int score,
        int total_questions,
        int correct_answers,
        int incorrect_answers,
        JSONArray qcm
    ){
        JSONObject response = new JSONObject();
        JSONObject data = new JSONObject();
        response.put("type", "QCM_CORRECTION");
        response.put("score", score);
        response.put("total_questions", total_questions);
        response.put("correct_answers", correct_answers);
        response.put("incorrect_answers", incorrect_answers);
        data.put("qcm", qcm);
        response.put("data", data);

        return response;
    }

    /*
    {
      "type": "USER_HISTORY",
      "data":{
        "users":[
          {
            "email": "test@test.fr",
            "qcm": [
              {
                "qcm_id": 0,
                "score": 1,
                "question_count": 2
              },
              //etc.
            ]
          },
          //etc.
        ]
      }
    }
     */
    /*
     * On va parcourir le fichier history.json et afficher l'historique des utilisateurs, ainsi que leur moyenne des scores
     */
    public static void print_history() {
        try {
            // Lire le fichier history.json à partir des ressources
            InputStream inputStream = JSONtools.class.getResourceAsStream("/history.json");
            if (inputStream == null) {
                System.err.println("Le fichier history.json n'existe pas dans les ressources.");
                return;
            }
            String content = new String(inputStream.readAllBytes(), StandardCharsets.UTF_8);
            JSONObject history = new JSONObject(content);

            // On récupère le JSON du fichier history.json
            JSONArray users = history.getJSONArray("users");
            for (int i = 0; i < users.length(); i++) {
                JSONObject user = users.getJSONObject(i);
                String email = user.getString("email");
                JSONArray qcm = user.getJSONArray("qcm");
                int score = 0;
                int question_count = 0;
                for (int j = 0; j < qcm.length(); j++) {
                    JSONObject qcm_data = qcm.getJSONObject(j);
                    score += qcm_data.getInt("score");
                    question_count += qcm_data.getInt("question_count");
                }
                double averageScore = question_count > 0 ? (double) score / question_count : 0;
                System.out.println("Email: " + email);
                System.out.println("Total Score: " + score + "/" + question_count);
                System.out.println("Average Score: " + averageScore);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /*
     * Met à jour le fichier history.json avec les informations du QCM pour un utilisateur donné
     */
    public static void update_history(String email, int qcmId, int score, int questionCount) {
        try {
            // Lire le fichier history.json à partir des ressources
            InputStream inputStream = JSONtools.class.getResourceAsStream("/history.json");
            if (inputStream == null) {
                System.err.println("Le fichier history.json n'existe pas dans les ressources.");
                return;
            }
            String content = new String(inputStream.readAllBytes(), StandardCharsets.UTF_8);
            JSONObject history = new JSONObject(content);

            // On récupère le JSON du fichier history.json
            JSONArray users = history.getJSONArray("users");
            boolean userFound = false;

            for (int i = 0; i < users.length(); i++) {
                JSONObject user = users.getJSONObject(i);
                if (user.getString("email").equals(email)) {
                    userFound = true;
                    JSONArray qcmArray = user.getJSONArray("qcm");
                    boolean qcmFound = false;

                    for (int j = 0; j < qcmArray.length(); j++) {
                        JSONObject qcm = qcmArray.getJSONObject(j);
                        if (qcm.getInt("qcm_id") == qcmId) {
                            // Mettre à jour les informations du QCM
                            qcm.put("score", score);
                            qcm.put("question_count", questionCount);
                            qcmFound = true;
                            break;
                        }
                    }

                    if (!qcmFound) {
                        // Ajouter un nouveau QCM
                        JSONObject newQcm = new JSONObject();
                        newQcm.put("qcm_id", qcmId);
                        newQcm.put("score", score);
                        newQcm.put("question_count", questionCount);
                        qcmArray.put(newQcm);
                    }

                    break;
                }
            }

            if (!userFound) {
                // Ajouter un nouvel utilisateur
                JSONObject newUser = new JSONObject();
                newUser.put("email", email);

                JSONArray qcmArray = new JSONArray();
                JSONObject newQcm = new JSONObject();
                newQcm.put("qcm_id", qcmId);
                newQcm.put("score", score);
                newQcm.put("question_count", questionCount);
                qcmArray.put(newQcm);

                newUser.put("qcm", qcmArray);
                users.put(newUser);
            }

            // Écrire les modifications dans le fichier history.json
            Files.write(Paths.get("src/main/resources/history.json"), history.toString().getBytes(), StandardOpenOption.TRUNCATE_EXISTING);
            System.out.println("Mise à jour du fichier history.json réussie.");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
