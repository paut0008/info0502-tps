package edu.info0502.tp4.server;

import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;

public class Question {
    private String title;
    private int id;
    private ArrayList<Proposition> propositions;

    public Question(int id, String title) {
        this.id = id;
        this.title = title;
        this.propositions = new ArrayList<>();
    }

    public Question(int id, String title, ArrayList<Proposition> propositions) {
        this.id = id;
        this.title = title;
        this.propositions = propositions;
    }

    public void addProposition(Proposition proposition) {
        propositions.add(proposition);
    }

    public int getId() { return id; }
    public String getTitle() { return title; }
    public ArrayList<Proposition> getPropositions() { return propositions; }

    public JSONObject toJson() {
        JSONObject json = new JSONObject();
        json.put("id", id);
        json.put("title_question", title);

        JSONArray answersArray = new JSONArray();
        for (Proposition proposition : propositions) {
            answersArray.put(proposition.toJson());
        }
        json.put("answers", answersArray);

        return json;
    }

    public boolean isCorrect(String userChoice) {
        return propositions.stream()
                .anyMatch(p -> p.getId() == Integer.parseInt(userChoice) && p.correction());
    }

    @Override
    public String toString() {
        return "Question{title='" + title + "', propositions=" + propositions + "}";
    }
}

class Proposition {
    private int id;
    private String text;
    private boolean isCorrect = false;
    private Boolean chosen;

    public Proposition(int id, String text) {
        this.id = id;
        this.text = text;
        this.isCorrect = false;
        this.chosen = null; // Initialiser à null
    }

    public Proposition(int id, String text, boolean isCorrect) {
        this.id = id;
        this.text = text;
        this.isCorrect = isCorrect;
        this.chosen = null; // Initialiser à null
    }

    public int getId() { return id; }
    public String getText() { return text; }
    public boolean isCorrect() { return isCorrect; }
    public Boolean getChosen() { return chosen; }
    public void setChosen(Boolean chosen) { this.chosen = chosen; }

    public boolean correction() {
        return chosen != null && isCorrect == chosen;
    }

    public JSONObject toJson() {
        JSONObject json = new JSONObject();
        json.put("id", id);
        json.put("answer", text);
        return json;
    }

    @Override
    public String toString() {
        return "Proposition{id='" + id + "', text='" + text + "', isCorrect=" + isCorrect + "}";
    }
}
