import java.util.Vector;

public class Mediatheque {
    private String proprietaire;
    private Vector<Media> collection;

    public Mediatheque() {
        this.proprietaire = "";
        this.collection = new Vector<Media>();
    }

    public Mediatheque(Mediatheque m) {
        this.proprietaire = m.getProprietaire();
        this.collection = new Vector<>(m.getCollection()); 
    }

    public void add(Media media) {
        this.collection.add(media);
    }

    public String getProprietaire() {
        return proprietaire;
    }

    public void setProprietaire(String proprietaire) {
        this.proprietaire = proprietaire;
    }

    public Vector<Media> getCollection() {
        return collection;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Propriétaire: ").append(proprietaire).append("\n");
        sb.append("Médias dans la collection : \n");
        
        for (Media media : collection) {
            sb.append(media.toString()).append("\n");
        }
        return sb.toString();
    }
}
