public class Livre extends Media {
    private String auteur;
    private String genre;
    private int nbpages;
    

    public Livre(String titre, StringBuffer cote, int note, String auteur, String genre, int nbpages){
        super(titre, cote, note);
        this.auteur = auteur;
        this.genre = genre;
        this.nbpages = nbpages;
    }

    public Livre(){
        this("", new StringBuffer(), 0, "", "", 0);
    }

    public Livre(Livre l){
        this(l.getTitre(), new StringBuffer(l.getCote()), l.getNote(), l.getAuteur(), l.getGenre(), l.getNbPages());
    }

    public String getAuteur(){ return auteur; }
    public String getGenre(){ return genre; }
    public int getNbPages(){ return nbpages; }

    public void setAuteur(String auteur){ this.auteur = auteur; }
    public void setGenre(String genre){ this.genre = genre; }
    public void setNbPages(int nbpages){ this.nbpages = nbpages; }
    
    @Override
    public Livre clone(){
        return new Livre(this);
    }
    
    @Override
    public boolean equals(Object obj){
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Livre l = (Livre)(obj);
        return (this.getTitre() == l.getTitre() && this.getCote().equals(l.getCote()) && this.getNote() == l.getNote() && this.auteur == l.auteur && this.genre == l.genre && this.nbpages == l.nbpages);
    }

    @Override
    public String toString() {
        return super.toString() + ", Auteur: " + auteur + ", Genre: " + genre + "Nombres de pages: " + nbpages + "";
    }

}