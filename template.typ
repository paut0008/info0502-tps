#let module = "Info0502"
#let tp = "TP1"
#let titre = "Le retour de JAVA"
#let auteur = "Alban PAUTRÉ"
#let groupe = "S5-1B"

#set text(
  font: "Fira Code",
  size: 12pt
)
#set page(
  paper: "a4",
  margin: (x: 2.5cm, y: 2.5cm),
)
#align(center)[
  #text(size: 28pt, weight: "bold")[#module #tp]
  
  #text(size: 20pt)[#titre]
]

Dernière modification faite le #datetime.today().display("[day]/[month]/[year]")

\
#auteur\
#groupe\
Etudiant à l'UFR SEN de Reims

\
\
\
#align(center)[
  #grid(
    columns: 2, 
    column-gutter: 2mm,
    image("urca.png", width: 75%),
    image("sen.png", width: 47%),
  )
]

#pagebreak()

#set text(
  font: "Comic Neue",
  size: 11pt,
  weight: "medium"
)
#set par(
  justify: true,
  leading: 0.52em,
)

#set page(
  paper: "a4",
  margin: (x: 2.5cm, y: 2.5cm),
  header: align(center)[#titre],
  footer: text[#auteur #h(2fr) #module],
  number-align: center,
)